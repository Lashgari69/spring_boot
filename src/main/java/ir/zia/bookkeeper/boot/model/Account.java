package ir.zia.bookkeeper.boot.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "account")
@Table(name = "account")
public class Account {
    @SequenceGenerator(name = "accountSeq", sequenceName = "ACCOUNT_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "accountSeq")
    private Long id;
    @Column(columnDefinition = "VARCHAR2(30)",unique = true,nullable = false)
    private String username;
    @Column(columnDefinition = "VARCHAR2(50)")
    private String email;
    @Column(columnDefinition = "VARCHAR2(100)")
    private String password;
    private int enabled;
    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "ACCOUNT_ID")
    private List<Role> role = new ArrayList<>();

    public Account() {}

    public Account(Long id, String username, String email, String password,String role, int enabled) {
        this.username = username;
        this.password = password;
    }

    public List<Role> getRole() {
        return role;
    }

    public Account setRole(List<Role> role) {
        this.role = role;
        return this;
    }

    public Long getId() {
        return id;
    }

    public Account setId(Long id) {
        this.id = id;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public Account setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Account setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Account setPassword(String password) {
        this.password = password;
        return this;
    }

    public int getEnabled() {
        return enabled;
    }

    public Account setEnabled(int enabled) {
        this.enabled = enabled;
        return this;
    }
}

