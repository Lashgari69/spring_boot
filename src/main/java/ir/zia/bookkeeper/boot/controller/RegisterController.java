package ir.zia.bookkeeper.boot.controller;

import ir.zia.bookkeeper.boot.model.Account;
import ir.zia.bookkeeper.boot.model.Role;
import ir.zia.bookkeeper.boot.service.AccountService_Impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.ITemplateEngine;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/register.do")
public class RegisterController {
    @Autowired
    private AccountService_Impl accountService;

    @RequestMapping("/save")
    public String check(@ModelAttribute Account account, Model model){
        List<Object> userPresentObj = accountService.isUserPresent(account.getEmail());
        if((Boolean) userPresentObj.get(0)) {
            model.addAttribute("successMessage", userPresentObj.get(1));
            return "account/register";
        }

        Role role = new Role("USER");
        account.getRole().add(role);
        accountService.save(account);
        return "redirect:/register.do/findAll";
    }

    @RequestMapping("/update")
    public String update(@ModelAttribute Account account) {
//        Role role = new Role("ROLE_USER");
//        account.getRole().add(role);
        accountService.update(account);
        return "redirect:/register.do/findAll";
    }

    @RequestMapping("/remove")
    public String remove(@ModelAttribute Account account) {
        accountService.remove(account);
        return "redirect:/register.do/findAll";
    }

    @RequestMapping("/findAll")
    public String findAll(HttpServletRequest request) {
        request.setAttribute("users", accountService.findAll(Account.class));
        return "account/register";
    }
}
