package ir.zia.bookkeeper.boot.repository;

import ir.zia.bookkeeper.boot.model.Role;
import org.springframework.stereotype.Repository;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class JpaRepository<T, I>{
    @PersistenceContext
    private EntityManager entityManager;

    public void save(T t) {
        entityManager.persist(t);
    }

    public void update(T t) {
        entityManager.merge(t);
    }

    public void remove(T t) {
        entityManager.remove(entityManager.merge(t));
    }

    public T findOne(Class<T> tClass, I id) {
        return entityManager.find(tClass, id);
    }

    public List findAll(Class<T> tClass) {
        Entity entity = tClass.getAnnotation(Entity.class);
        Query query = entityManager.createQuery("select entity from "+ entity.name() +" entity");
        return query.getResultList();
    }
//
//    public T findByName(String name, Class C) {
//        Query query = entityManager.createQuery("SELECT X from acc X where X.username=:NAME");
//        query.setParameter("NAME", name);
//        Object object = query.getSingleResult();
//        return (T) object;
//    }
//
    public List<String> getRole(Class<T> tClass, Long acc) {
        Entity entity = tClass.getAnnotation(Entity.class);
        Query query = entityManager.createQuery("SELECT role from "+ entity.name() +" where ACCOUNT_ID=:acc");
        query.setParameter("acc", acc);
        return query.getResultList();
    }

}
