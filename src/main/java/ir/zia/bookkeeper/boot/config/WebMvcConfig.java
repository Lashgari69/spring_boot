package ir.zia.bookkeeper.boot.config;

import ir.zia.bookkeeper.boot.controller.RequestMonitor;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("homepage");
        registry.addViewController("/error").setViewName("error");
        registry.addViewController("/register").setViewName("account/register");
        registry.addViewController("/login").setViewName("/account/login");
        registry.addViewController("/user").setViewName("user/index");
        registry.addViewController("/admin").setViewName("admin/index");
    }

    @Bean
    public FilterRegistrationBean<RequestMonitor> loggingFilter(){
        FilterRegistrationBean<RequestMonitor> registerBean = new FilterRegistrationBean<>();

        registerBean.setFilter(new RequestMonitor());
        registerBean.addUrlPatterns("/user/*","/admin/*");
        registerBean.setOrder(1);

        return registerBean;
    }


}
