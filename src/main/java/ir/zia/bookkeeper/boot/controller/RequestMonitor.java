package ir.zia.bookkeeper.boot.controller;

import ir.zia.bookkeeper.boot.model.Account;
import ir.zia.bookkeeper.boot.model.Role;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;

@Component
@Order(1)
public class RequestMonitor implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //todo
    }

    @Override
    public void doFilter(final ServletRequest req, final ServletResponse resp, final FilterChain chain) throws IOException, ServletException {
            HttpServletRequest request = (HttpServletRequest) req;
            HttpServletResponse response = (HttpServletResponse) resp;
            if (request.getSession().getAttribute("loginData") == null ){
                response.sendRedirect("/error");
            } else {
                String panelName = request.getRequestURI().split("/")[1];
                if (((Account) request.getSession().getAttribute("loginData")).getRole().contains(panelName.toUpperCase()))
                {
                    chain.doFilter(req, resp);
                } else {
                    response.sendRedirect("/error");
                }
            }

        }

    @Override
    public void destroy() {
        //todo
    }


}
