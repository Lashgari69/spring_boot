package ir.zia.bookkeeper.boot.repository;

import ir.zia.bookkeeper.boot.model.Account;
import ir.zia.bookkeeper.boot.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<Account, Long> {
    Optional<Account> findByEmail(String email);
    public Account findByEmailAndPassword(String email, String password);

}
