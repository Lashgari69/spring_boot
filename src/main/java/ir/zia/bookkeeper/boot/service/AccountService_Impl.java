package ir.zia.bookkeeper.boot.service;

import ir.zia.bookkeeper.boot.model.Account;
import ir.zia.bookkeeper.boot.model.Role;
import ir.zia.bookkeeper.boot.repository.JpaRepository;
import ir.zia.bookkeeper.boot.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class AccountService_Impl implements UserService{

    @Autowired
    private JpaRepository jpaRepository;

    @Autowired
    UserRepository userRepository;

    @Transactional
    public void save(Account account) {
        jpaRepository.save(account);
    }

    @Transactional
    public void update(Account account) {
        jpaRepository.update(account);
    }

    @Transactional
    public void remove(Account account) {
        jpaRepository.remove(account);
    }


    public Account findOne(Account account){
        return (Account) jpaRepository.findOne(Account.class, account.getId());
    }

    public List<Account> findAll(Class<Account> accountClass) {
        return jpaRepository.findAll(Account.class);
    }

    @Override
    public List<Object> isUserPresent(String email) {
        boolean userExist = false;
        String msg = null;
        Optional<Account> existingUserEmail = userRepository.findByEmail(email);
        if (existingUserEmail.isPresent()){
            userExist = true;
            msg = " Email Already Present !";
        }
        return Arrays.asList(userExist, msg);
    }

    @Override
    public Account login(Account account) {
        Account userSession = userRepository.findByEmailAndPassword(account.getEmail(), account.getPassword());
        userSession.setRole(findRole(userSession.getId()));
        return userSession;
    }

    @Override
    public List<Role> findRole(Long id){
        List<Role> roleLost=new ArrayList<>();
        roleLost= jpaRepository.getRole(Role.class, id);
        return roleLost;
    }
}
