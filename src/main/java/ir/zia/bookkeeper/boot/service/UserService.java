package ir.zia.bookkeeper.boot.service;

import ir.zia.bookkeeper.boot.model.Account;
import ir.zia.bookkeeper.boot.model.Role;

import java.util.List;

public interface UserService {
    public List<Object> isUserPresent(String email);
    public Account login(Account account);
    public List<Role> findRole(Long id);

}
