package ir.zia.bookkeeper.boot.controller;

import ir.zia.bookkeeper.boot.model.Account;
import ir.zia.bookkeeper.boot.service.AccountService_Impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@Controller
public class LoginController {

    @Autowired
    private AccountService_Impl accountService;

    @RequestMapping("/login.do")
    public String login(@ModelAttribute Account account, HttpSession session) {
        Account user = accountService.login(account);
        session.setAttribute("loginData", accountService.login(account));

        String url = "";
        if (user.getRole().contains("ADMIN")) {
            url = "admin";
        }else{
            url = "user";
        }
        return "redirect:"+url;
    }

    @RequestMapping("/logout.do")
    public String logout(HttpSession session ) {
        session.invalidate();
        return "redirect:/login";
    }
}
