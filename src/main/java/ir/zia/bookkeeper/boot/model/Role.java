package ir.zia.bookkeeper.boot.model;

import javax.persistence.*;

@Entity(name = "role")
@Table(name = "role")
public class Role {
    @SequenceGenerator(name = "roleSeq", sequenceName = "ROLE_SEQ" , allocationSize = 1)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "roleSeq")
    private int id;
    @Column(columnDefinition = "VARCHAR2(20)")
    private String role;

    public Role() {}

    public Role(int id,String role) {
        this.id = id;
        this.role = role;
    }

    public Role(String role) {
        this.role = role;
    }

    public int getId(){
        return id;
    }

    public Role setId(int id) {
        this.id = id;
        return this;
    }

    public String getRole() {
        return role;
    }

    public Role setRole(String role) {
        this.role = role;
        return this;
    }
}
